# coding:utf-8
# Author:Edison
# Time:2018-08-09 14:39
import time
import datetime
import sys
import os
import shutil
import subprocess
import serial
import wmi
from configparser import ConfigParser
from apis.debug import DebugTools
from apis.report import Report
from apis.general_config import CheckandCreate, log_config
from apis.ftp_client import ftp_run
from apis.mails import MailAgent, MimeMsg
from apis.query_utils import get_candidate_dates
from apis.transform_unit import simplify_and_compress_zephir_10min, simplify_and_compress_lidar_10min

from error import LidarFtpError, RawDataRsyncError, LidarCorrError, _10minDataRsyncError

sys.path.append(os.path.join(os.getcwd(), 'apis'))
rootpath = os.path.join(os.getcwd())

report_config = ConfigParser()
report_config.read('{}/configs/{}'.format(rootpath, 'dailyreport.conf'))
project_name = report_config.get('installation', 'project_name')

logger = log_config()


if __name__ == '__main__':
    """
    1、通过ftp从lidar pc上指定目录增量下载lidar文件到本地指定目录
    2、rsync从data pc上指定目录增量同步拉取Motion, Trimble, Weather数据到本地指定目录
    3、调用校准程序，校准lidar,Motion, Trimble, Weather数据，生成10min数据到本地指定目录
    4、rsync将本地的10min数据，同步增量推送至云服务器的指定目录，同时生成10min压缩文件
    """
    try:
        # 0、初始化一些中间文件目录
        cac = CheckandCreate()
        cac.check()

        # 1、通过ftp从lidar pc上指定目录增量下载lidar文件到本地指定目录
        logger.info(80 * '=')
        logger.info(u">>>>start sync lidar data from lidar pc via ftp")
        try:
            ftp_run()
        except Exception as e:
            # raise LidarFtpError(str(e))
            logger.error(str(e))
            # send error_msgs by mail
            MailAgent().send_mail(message=MimeMsg(str(e)).mime)
        logger.info(u">>>>finish sync lidar data from lidar pc via ftp")

        # 2、rsync从data pc上增量同步拉取Motion, Trimble, Weather数据到本地指定目录
        logger.info(80 * '=')
        logger.info(u">>>>start sync Motion, Trimble, Weather data from data pc via rsync")
        try:
            raw_src_dir = report_config.get('installation', 'data_pc_rsync_dir')
            
            raw_des_dir_parse = "/".join(report_config.get('general', 'datapath').lower().split(':\\'))
            raw_des_dir = "/cygdrive/{}/{}".format(raw_des_dir_parse, (report_config.get('general', 'list_buoys')))
            
            _root_dir = "/".join(rootpath.lower().split(':\\'))

            raw_data_rsync_cmd = (
                'rsync -vzrt --progress --partial '
                '--include-from=/cygdrive/{}/lidar_rsync_clude.txt '
                '--exclude-from=/cygdrive/{}/lidar_rsync_clude.txt '
                '{} {}').format(_root_dir, _root_dir, raw_src_dir, raw_des_dir)
            logger.info(raw_data_rsync_cmd)
            
            try:
                raw_data_rsync_output = subprocess.check_output(raw_data_rsync_cmd, stderr=subprocess.STDOUT, timeout=60)
            except subprocess.CalledProcessError as ce:
                raise RawDataRsyncError(str(ce.output))
            except subprocess.TimeoutExpired as te:
                raise RawDataRsyncError(str(te.output))
            logger.info(raw_data_rsync_output.decode('utf-8'))

        except Exception as e:
            # raise RawDataRsyncError(str(e))
            logger.error(str(e))
            # send error_msgs by mail
            MailAgent().send_mail(message=MimeMsg(str(e)).mime)
        logger.info(u">>>>finish sync Motion, Trimble, Weather data from data pc via rsync")

        # 3、校准原始数据，生成10min数据
        logger.info(80 * '=')
        logger.info(u">>>>start lidar data correction")
        is_corr_error_occur = False
        corr_error_msgs = {}

        # get candidata dates from remote
        dates =  set([])
        try:
            dates = get_candidate_dates()
        except Exception as e:
            logger.error(str(e))
            MailAgent().send_mail(message=MimeMsg(str(e)).mime)
        
        date_delta = int(report_config.get('installation', 'date_delta'))
        current_date = (datetime.datetime.today() - datetime.timedelta(days=date_delta)).strftime('%Y-%m-%d')
        dates.add(current_date)
        for date_ in dates:
            try:
                r = Report()
                r.dreport_internal(date_)
            except Exception as e:
                is_corr_error_occur = True
                corr_error_msgs[date_] = str(e)
        try:
            if is_corr_error_occur:
                corr_error_msgs_str = '\n'.join(
                    ['[{}]-{}'.format(k, str(v)) for (k, v) in corr_error_msgs.items()]
                    )
                raise LidarCorrError(corr_error_msgs_str)       
        except Exception as e:
            logger.error(str(e))
            # send error_msgs by mail
            MailAgent().send_mail(message=MimeMsg(str(e)).mime)
      
        logger.info(u">>>>finish lidar data correction")

        """
        4、处理本地的校准得到的10min数据：
        a. 将/out目录下的10min数据拷贝到sbd_upload/simplified目录
        b. 将/out目录下的10min数据通过rsync同步增量推送至云服务器的指定目录
        """
        logger.info(80 * '=')
        logger.info(u">>>>start sync 10min data to ecs via rsync")
        try:
            _10min_des_dir = report_config.get('installation', 'ecs_rsync_dir')
            _10min_src_dir_parse = "/".join(rootpath.lower().split(':\\'))
            _10min_src_dir = "/cygdrive/{}/{}/{}/".format(
                _10min_src_dir_parse, 
                report_config.get('installation', 'outpath'), 
                report_config.get('general', 'list_buoys'))
            _10min_data_rsync_cmd = (
                'rsync -vzrt --progress --partial '
                '--include=*_10MinAveraged_* '
                '--exclude=/* '
                '{} {}').format(_10min_src_dir, _10min_des_dir)
            logger.info(_10min_data_rsync_cmd)
            try:
                cur_10min_src_dir = '{}/{}/{}'.format(
                    rootpath, 
                    report_config.get('installation', 'outpath'), 
                    report_config.get('general', 'list_buoys')
                )
                # copy successful 10min corrected data files to sbd upload dir
                sbd_upload_10min_src_dir = '{}/{}'.format(
                    rootpath, 
                    report_config.get('installation', 'sbd_upload_path')
                )
                for _10min_filename in os.listdir(cur_10min_src_dir):
                    if '10MinAveraged' in _10min_filename:
                        simplify_and_compress_zephir_10min('{}/{}'.format(cur_10min_src_dir, _10min_filename), sbd_upload_10min_src_dir)
                # for failure corrected file, copy the 10min zph file and error log file to sbd upload dir
                raw_path = report_config.get('installation', 'rawpath')
                raw_lidar_path = '{}/{}/LIDAR'.format(
                    raw_path,
                    report_config.get('general', 'list_buoys')
                )
                raw_lidar_list = os.listdir(raw_lidar_path)
                for (date, err_msg) in corr_error_msgs.items():
                    error_log_file = '{}/{}_{}_{}_corr.log'.format(
                        sbd_upload_10min_src_dir, date, project_name, 
                        report_config.get('general', 'list_buoys')
                        )
                    with open(error_log_file, 'w') as f:
                        f.write(err_msg)
                    raw_lidar_date = time.strftime('Y%Y_M%m_D%d', time.strptime(date, '%Y-%m-%d'))
                    for raw_lidar_filename in raw_lidar_list:
                        if raw_lidar_filename.startswith('Wind10') and raw_lidar_filename.endswith('zip') and raw_lidar_date in raw_lidar_filename:
                            simplify_and_compress_lidar_10min('{}/{}'.format(raw_lidar_path, raw_lidar_filename), sbd_upload_10min_src_dir)        
                # rsync successful 1raw_lidar_filename0min corrected data files to remote ecs
                _10min_data_rsync_output = subprocess.check_output(_10min_data_rsync_cmd, stderr=subprocess.STDOUT, timeout=60)
                # if success sync, move the successful corrected data files to backup dir
                backup_10min_src_dir = '{}/{}'.format(
                    rootpath, 
                    report_config.get('installation', 'backup_outpath')
                )
                for _10min_filename in os.listdir(cur_10min_src_dir):
                    shutil.move(
                        '{}/{}'.format(cur_10min_src_dir, _10min_filename),
                        '{}/{}'.format(backup_10min_src_dir, _10min_filename),
                    )
            except subprocess.CalledProcessError as ce:
                raise _10minDataRsyncError(str(ce.output))
            except subprocess.TimeoutExpired as te:
                raise _10minDataRsyncError(str(te.output))
            logger.info(_10min_data_rsync_output.decode('utf-8'))
        except Exception as e:
            raise _10minDataRsyncError(str(e))
        logger.info(u">>>>finish sync 10min data to ecs via rsync")
    except Exception as e:
        # send error_msgs by mail
        logger.error(str(e))
        MailAgent().send_mail(message=MimeMsg(str(e)).mime)
    finally:
        '''
        5.通知核心控制板关闭电源
        否则每天自动启动的设备一直处于上电状态
        '''
        logger.info(80 * '=')
        logger.info(u">>>>notice core controller to shutdown")
        datadisk = report_config.get('installation', 'datapath')
        c = wmi.WMI()
        for physical_disk in c.Win32_DiskDrive():
            for partition in physical_disk.associators("Win32_DiskDriveToDiskPartition"):
                for logical_disk in partition.associators("Win32_LogicalDiskToPartition"):
                    if logical_disk.Caption == datadisk:
                        FreeSpace = int(logical_disk.FreeSpace)/1024/1024
                        break
        with serial.Serial(port='COM1', baudrate=19200, timeout=5) as sc:
            diskspace = str.encode("%d"%FreeSpace)
            sc.write(diskspace)
        logger.info("FreeSpace:%d"%FreeSpace)
        logger.info(u">>>>finish notice core controller to shutdown")
        
        logger.info(u">>>>auto shut down self at {}".format(datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')))


