# coding:utf-8


class Error(Exception):
    status_code = 100
    message = 'Common error.'

    def __init__(self, msg=None):
        if msg is not None:
            self.message = msg
        super(Exception, self).__init__(self.message, self.status_code)


class LidarFtpError(Error):
    status_code = 100
    message = 'Lidar data ftp download error.'


class RawDataRsyncError(Error):
    status_code = 200
    message = 'Raw data(Motion, Trimble, Weather) rsync error.'


class LidarCorrError(Error):
    status_code = 300
    message = 'Lidar correction error.'


class _10minDataRsyncError(Error):
    status_code = 400
    message = '10min data rsync error.'
