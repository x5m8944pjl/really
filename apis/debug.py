import os
import shutil

from apis.general_config import Configure, log_config

logger = log_config('report.log')


def delete_unit(path):
    try:
        logger.info('delete file: {}'.format(os.path.abspath(path)))
        os.remove(path)
    except Exception as e:
        logger.error('error: {}'.format(e))


def delete_file_in_dir(dir_path):
    logger.info('select dir: {}'.format(dir_path))
    for file in os.listdir(dir_path):
        file_path = os.path.join(dir_path, file)
        try:
            delete_files(file_path)
        except Exception as e:
            logger.error('error :{}'.format(e))


def delete_files(path):
    if os.path.isdir(path):
        for dir in os.listdir(path):
            dir_path = os.path.join(path, dir)
            if os.path.isdir(dir_path):
                delete_file_in_dir(dir_path)
            elif os.path.isfile(dir_path):
                delete_unit(dir_path)
    elif os.path.isfile(path):
        delete_unit(path)


class DebugTools(object):
    def __init__(self):
        self.config = Configure()
        self.config.readconf('dailyreport.conf')
        self.root = self.config.get('installation', 'root')
        self.sharedpath = self.config.get('installation', 'sharedpath')
        self.outpath = self.config.get('installation', 'outpath')
        self.reportpath = self.config.get('installation', 'reportpath')
        self.temppath = self.config.get('installation', 'temppath')

    def delete_shared(self, date=None, buoyid=None):
        if buoyid is None and date is None:
            delete_file_in_dir(self.sharedpath)

    def delete_out(self, date=None, buoyid=None):
        if date is None and buoyid is None:
            delete_file_in_dir(self.outpath)

    def delete_report(self, date=None, buoyid=None):
        if date is None and buoyid is None:
            delete_file_in_dir(self.reportpath)

    def delete_temp(self, date=None, buoyid=None):
        if date is None and buoyid is None:
            delete_file_in_dir(self.temppath)

    def delete(self, method='all'):
        if method == 'all':
            self.delete_shared()
            self.delete_out()
            self.delete_report()
            self.delete_temp()
        if method == 'except_out':
            self.delete_shared()
            self.delete_report()
            self.delete_temp()
