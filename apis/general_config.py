# Author:Edison
# Time:2018-08-07 14:24
from configparser import ConfigParser
import logging
import os
import sys
import pandas as pd
import numpy as np
import time
import apis.time_utils as time_utils

sys.path.append(os.path.join('../'))
rootpath = os.path.join(os.getcwd())


# rootpath =os.path.join('../')

class Configure(ConfigParser):
    """
    get configure file

    """

    def readconf(self, confname):
        self.read('{}/configs/{}'.format(rootpath, confname))
        return self


class CheckandCreate(object):
    """
    初次软件时执行，检查general_config.conf中配置并创建相关文件

    """

    def __init__(self):
        self.conf = Configure()
        self.conf.readconf(confname='dailyreport.conf')

        # self.files = ['log/db.log']
        # self.dirs = ['report','temp','out/cleardata','coda','lidar','trimble','weather']

    def checkfile(self, filepath):
        if not os.path.exists(filepath):
            os.system('echo log>{}'.format(filepath))

    def checkdir(self, dirpath):
        if not os.path.isdir(dirpath):
            os.makedirs(dirpath)

    def read_configs(self):
        files = [n for n in self.conf.get('installation', 'log').split(' ')]
        root_path = [n for n in self.conf.get('installation', 'root').split(' ')]
        jardepend = [n for n in self.conf.get('general', 'jardepend').split(' ')]
        outpath = [n for n in self.conf.get('installation', 'outpath').split(' ')]
        backup_outpath = [n for n in self.conf.get('installation', 'backup_outpath').split(' ')]
        sbd_upload_path = [n for n in self.conf.get('installation', 'sbd_upload_path').split(' ')]
        list_lidar = [n for n in self.conf.get('general', 'list_buoys').split(' ')]
        dir_list = root_path + jardepend + outpath + backup_outpath + sbd_upload_path
        for lidar in list_lidar:
            dir_list.append('{}/{}'.format(outpath[0], lidar))
        # print (dir_list)
        return files, dir_list

    def check(self):
        self.files, self.dirs = self.read_configs()
        for file in self.files:
            self.checkfile('{}/{}'.format('log', file))
        for dir in self.dirs:
            self.checkdir(dir)


def log_config(logname='report.log'):
    """
    log configure

    :return:
    """
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(filename)s [line:%(lineno)d] %(funcName)s %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        filemode='a',
                        # 如果不想在命令行中出现log信息，将下面这行代码取消注释
                        # filename='{}/log/{}'.format(rootpath, logname),
                        )
    logger = logging.getLogger(logname)

    if not logger.handlers:
        handler = logging.FileHandler(
            filename='{}/log/{}'.format(rootpath, logname),
            encoding='utf-8')
        formatter = logging.Formatter(
            '%(asctime)s %(filename)s[line:%(lineno)d] %(funcName)s %(levelname)s %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    # logger.handlers.pop()
    return logger


def time2stamp(_time):
    stamp = int(time.mktime(time.strptime(_time, "%Y_%m_%d")))
    return stamp


def stamp2time(_stamp):
    return time.strftime("%Y_%m_%d", time.localtime(_stamp))


def time_list(start, end, alpha=86399):
    if start == end:
        return [start]
    else:
        start = time2stamp(start)
        end = time2stamp(end)
        tl = []
        tmp = start
        while tmp < end:
            tmp = start + alpha
            tl.append(stamp2time(start))
            start = tmp + 1
        return tl


class Zephir10minData(object):
    def __init__(self, file_path, buoy_id):
        self.df = pd.read_csv(file_path, sep='\t', dtype={"Algorithm": str})
        self.config = Configure().readconf('dailyreport.conf')
        self.heights = self.config.get('general', 'set_heights').split(' ')
        self.col_map = self.get_col_map()
        self.df.rename(columns=self.col_map, inplace=True)
        self.df['ts'] = self.df['ts'].apply(lambda x: time_utils.tstr2dt(time_utils.utc2cst(x)))
        self.df['buoy_id'] = pd.Series(np.ones(len(self.df)) * buoy_id, index=self.df.index)
        self.__cleanup_data()

    def get_col_map(self):
        col_map = {'UTC': 'ts', 'Algorithms': 'alg', }
        for height in self.heights:
            col_map['{}.0 horizontal direction (TN) [°]'.format(height)] = 'dir_{}m'.format(height)
            col_map['{}.0 wind speed corrected [m/s]'.format(height)] = 'speed_{}m'.format(height)
            col_map['{}.0 min. wind speed [m/s]'.format(height)] = 'min_speed_{}m'.format(height)
            col_map['{}.0 max. wind speed [m/s]'.format(height)] = 'max_speed_{}m'.format(height)
            col_map['{}.0 stdDev. wind speed [m/s]'.format(height)] = 'stddev_speed_{}m'.format(height)
            col_map['{}.0 availability [%]'.format(height)] = 'availability_{}m'.format(height)
        return col_map

    def __cleanup_data(self):
        self.df = self.df.dropna(thresh=5)

    def df(self):
        return self.df


class ZephirRawData(object):
    def __init__(self, path_f, buoy_id):
        self.df = pd.read_csv(path_f, sep='\t', dtype={"Algorithm": str})
        self.df.rename(columns={'UTC': 'ts',
                                'Height [m]': 'height',
                                'Algorithm': 'alg',
                                'Horizontal direction (TN) [°]': 'horizontal_direction',
                                'Horizontal speed [m/s]': 'horizontal_speed',
                                'Vertical speed [m/s]': 'vertical_speed',
                                'Air temperature [°C]': 'air_temperature',
                                'Humidity [%]': 'humidity',
                                'Met direction [°]': 'met_direction',
                                'Met wind speed [m/s]': 'met_speed',
                                'Points in fit': 'points',
                                'Z-Bearing [deg]': 'z_bearing'}, inplace=True)

        self.df['ts'] = self.df['ts'].apply(lambda x: time_utils.tstr2dt(time_utils.utc2cst(x)))
        self.df['buoy_id'] = pd.Series(np.ones(len(self.df)) * buoy_id, index=self.df.index)
        self.__cleanup_data()

    def __cleanup_data(self):
        self.df = self.df.dropna(thresh=5)

    def df(self):
        return self.df


def main():
    """
    main fun of  file
    :return:
    """
    conf = Configure().readconf('dailyreport.conf')
    print([n for n in conf.items()])
    print(conf.get('general', 'heights'))


if __name__ == '__main__':
    main()
