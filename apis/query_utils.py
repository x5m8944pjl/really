import os, sys
import pymysql
from apis.general_config import Configure
sys.path.append(os.path.join(os.getcwd()))

cfg = Configure().readconf('dailyreport.conf')

host = str(cfg.get('remote_db', 'db_host'))
port = int(cfg.get('remote_db', 'db_port'))
user = cfg.get('remote_db', 'db_user')
password = cfg.get('remote_db', 'db_password')
database = cfg.get('remote_db', 'db_name')

def get_candidate_dates(host=host, port=port, user=user, password=password, database=database):
    dates = []
    conn = None
    cursor = None
    try:
        conn = pymysql.connect(
            host = host,
            port = port,
            user = user,
            password = password,
            database = database,
            charset = 'utf8',
            cursorclass = pymysql.cursors.DictCursor
        )
        cursor = conn.cursor()
        cursor.execute("SELECT date from candidate_dates ;")
        result = cursor.fetchall()
        dates = list(map(lambda r:r['date'], result))
    except Exception as e:
        dates = []
        raise e
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()
    return set(dates)