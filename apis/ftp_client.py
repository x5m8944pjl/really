# -*- coding: utf-8 -*-
import time, os, sys
import json
from ftplib import FTP

import logging
import logging.handlers
global logger

path = os.path.join(os.getcwd())
file_name = os.path.join(path, 'log/ftp.log')

logger = logging.getLogger()
handler = logging.handlers.RotatingFileHandler(
    file_name, mode='a', maxBytes=10 * 1024 * 1024, backupCount=1)
formatter = logging.Formatter(
    '[%(asctime)s] line:%(lineno)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

count = 0
interval = 240
MAX_RETRY_TIMES = 3
retry_times = 0

def LoadCfg(fpath):
    f = open(fpath)
    data = f.read()
    dic = json.loads(data)
    f.close()
    return dic

cfg = LoadCfg(os.path.join(path, 'configs/lidar_ftp.json'))

class myFTP(object):
    def __init__(self, ip, u, pwd, port, mode=True, timeout=60):
        self.user = u
        self.pwd = pwd
        self.ftp = FTP()
        self.ftp.connect(ip, port, timeout=timeout)
        self.login(self.user, self.pwd)
        self.ftp.set_pasv(mode)
        self.files = []

    def login(self, user, pwd):
        self.ftp.login(user, pwd)

    def setDebugLevel(self, level):
        self.ftp.set_debuglevel(level)

    def close(self):
        self.ftp.close()

    # --Filter file or dir. 
    # --Filter option: '-' or 'd'. 
    # --e.g. self.filter('d')

    def callback(self, line):
        self.files.append(line)

    def filter(self, path, f=None):
        rs = []
        self.files = []
        pwd = self.ftp.pwd()
        self.ftp.cwd(path)
        try:
            self.ftp.retrlines("LIST", self.callback)
        except Exception as e:
            self.ftp.retrlines("LIST", self.callback)
        if f:
            for file in self.files:
                if file.startswith(f):
                    rs.append(file.split()[-1].strip())
        else:
            for file in self.files:
                rs.append(file.split()[-1].strip())
        self.ftp.cwd(pwd)
        return rs

    def dirExists(self, d, path):
        if d in self.filter(path, 'd'):
            return True
        else:
            return False

    def fileExists(self, f, path):
        if os.path.basename(f) in self.filter(path, '-'):
            return True
        else:
            return False

    def uploadFile(self, f, path):
        global count, retry_times
        try:
            path = path.rstrip('/')
            remote_file = '/'.join([path, os.path.basename(f)])
            try:
                logger.info(u'Uploading file {}'.format(f))
                with open(f, 'rb') as fp:
                    self.ftp.storbinary('STOR {}'.format(remote_file), fp)
            except Exception as e:
                with open(f, 'rb') as fp:
                    self.ftp.storbinary('STOR {}'.format(remote_file), fp)
            count += 1
        except Exception as e:
            logger.exception(e)
            if retry_times >= MAX_RETRY_TIMES:
                retry_times = 0
                return
            retry_times += 1
            logger.error(u'Retry to upload file: {} after {} secs'.format(f, cfg['retry_interval']))
            time.sleep(cfg['retry_interval'])
            self.ftp.connect()
            self.ftp.login(self.user, self.pwd)
            self.uploadFile(f, path)

    def uploadDir(self, d, path):
        global cfg
        files = os.listdir(d)
        existsfile = self.filter(path, '-')
        files = list(set(files) - set(existsfile))
        files = sorted(files, key=lambda x: os.path.getmtime(os.path.join(d, x)), reverse=cfg['reverse'])
        path = path.rstrip('/')
        for f in files:
            if os.path.isfile(d + '/' + f):
                self.uploadFile(d + '/' + f, path)
            elif os.path.isdir(d + '/' + f):
                if not self.dirExists(f, path):
                    self.ftp.mkd(path + '/' + f)
                self.uploadDir(d + '/' + f, path + '/' + f)

    def downloadFile(self, file, ftp_path):
        global count, retry_times
        try:
            ftp_path = ftp_path.rstrip('/')
            remote_file = '/'.join([ftp_path, os.path.basename(file)])
            try:
                logger.info(u'Downloading file {}'.format(remote_file))
                with open(file, 'wb') as fp:
                    self.ftp.retrbinary('RETR {}'.format(remote_file), fp.write)
            except Exception as e:
                with open(file, 'wb') as fp:
                    self.ftp.retrbinary('RETR {}'.format(remote_file), fp.write)
            count += 1
        except Exception as e:
            logger.exception(e)
            if retry_times >= MAX_RETRY_TIMES:
                retry_times = 0
                return
            retry_times += 1
            logger.error(u'Retry to download file: {} after {} secs'.format(remote_file, cfg['retry_interval']))
            time.sleep(int(cfg['retry_interval']))
            self.ftp.connect()
            self.ftp.login(self.user, self.pwd)
            self.downloadFile(file, ftp_path)
    
    def downloadDir(self, local_path, ftp_path):
        _ftpFiles = []
        _localFiles = os.listdir(local_path)
        _ftptemps = self.filter(ftp_path, '-')
        _ftpDirs = self.filter(ftp_path, 'd')
        for f in _ftptemps:
            if f.endswith("zip"):
                _ftpFiles.append(f)
        _exfiles = list(set(_ftpFiles) - set(_localFiles))

        ftp_path = ftp_path.rstrip('/')
        for _file in _exfiles:
            self.downloadFile(os.path.join(local_path, _file), ftp_path)
        for _dir in _ftpDirs:
            if not os.path.exists(os.path.join(local_path, _dir)):
                os.mkdir(os.path.join(local_path, _dir))
            self.downloadDir(os.path.join(local_path, _dir), ftp_path + '/' + _dir)


def ftp_run():
    global count, interval, cfg
    ipAddr = cfg["ipAddr"]
    usrName = cfg["usr"]
    passWD = cfg["passwd"]
    port = cfg["port"]
    dir = cfg["local_path"]
    remote_path = cfg["remote_path"].lstrip('./').rstrip('/')
    debug_level = cfg["debug_level"]
    mode = None
    if cfg.get('pasv_mode'):
        mode = bool(cfg["pasv_mode"])
    interval = cfg["interval"]
    if not os.path.exists(dir):
        err_msg = u"Source dir: '{}' doesn't exists".format(dir)
        logger.info(err_msg)
        raise Exception(err_msg)
    count = 0
    start = int(time.time())
    ftp = myFTP(ipAddr, usrName, passWD, port, mode, cfg['timeout'])
    ftp.setDebugLevel(debug_level)
    if cfg['mode'].lower().startswith('up'):
        try:
            ftp.ftp.sendcmd('SITE UMASK {}'.format(cfg.get('umask', '000')))
        except Exception as e:
            logger.info('Unsupport cmd: SITE UMASK {}'.format(cfg.get('umask', '000')))
        ftp.uploadDir(dir, './{}'.format(remote_path))
    else:
        ftp.downloadDir(dir, './{}'.format(remote_path))
    ftp.close()
    end = int(time.time())
    hour = (end - start) / 3600
    min = (end - start - 3600 * hour) / 60
    sec = (end - start - 3600 * hour) % 60
    logger.info('transf finished, total %d files, time used: %02d:%02d:%02d' %
                (count, hour, min, sec))               

if __name__ == '__main__':
    while True:
        try:
            os.chdir(path)
            logger.info(u"---------------------------------------")
            logger.info(u"{} service started...".format(cfg['serv_name']))
            ftp_run()
            logger.info(u"{} service finished.".format(cfg['serv_name']))
        except Exception as e:
            logger.exception(e)
        logger.info(u"Sleep time: {} minutes".format(interval))
        time.sleep(int(interval) * 60)    