import os
import pandas as pd
from datetime import datetime, timedelta
from apis.general_config import Configure, log_config

config = Configure()
config.readconf('dailyreport.conf')
logger = log_config('report.log')


def timeutc8(filename, sep):
    with open(filename, 'r') as f:
        first_line = f.readline().replace('\t', ',')
        second_line = f.readline().replace('\t', ',')
        third_line = f.readline().replace('\t', ',')
    header = first_line + second_line + third_line
    df = pd.read_csv(filename, header=3, sep=sep)
    # print(df.head())
    # print(df.columns)
    # print(len(df.columns))
    # if filename.startswith('zephir') and filename.endswith('.csv') and ('10Min' in filename):
    df['UTC'] = df['UTC'].apply(lambda x: tstr2dt(utc2cst(x)))
    df = df.rename(columns={'UTC': 'UTC+8'})
    df.to_csv(filename, index=0)

    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(header + content)


def tab2comma(filename):
    with open(filename, 'r') as f:
        first_line = f.readline()
        second_line = f.readline()
        third_line = f.readline()
    header = first_line + second_line + third_line
    df = pd.read_csv(filename, header=3, sep='\t')
    df.to_csv(filename, index=0)

    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(header + content)


def modify10min(path):
    files = os.listdir(path)  # 得到文件夹下的所有文件名称
    for file in files:  # 遍历文件夹
        try:
            if file.startswith('zephir') and file.endswith('.csv') and ('10Min' in file):
                logger.info('modify file: {}'.format(file))
                tab2comma(os.path.join(path, file))
                timeutc8(os.path.join(path, file), sep=',')
            elif file.startswith('zephir') and file.endswith('.csv'):
                logger.info('modify file: {}'.format(file))
                timeutc8(os.path.join(path, file), sep='\t')
        except Exception as e:
            pass


def utc2cst(ts):
    ts = ts.replace('T', ' ')
    dt = datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
    dt = dt + timedelta(hours=8)
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def tstr2dt(time_string):
    ts = time_string.replace('T', ' ')
    try:
        return datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
    except:
        return datetime.strptime(ts, "%Y/%m/%d %H:%M:%S")


def dt2tstr(dt):
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def minus_10min(dt):
    return dt + timedelta(minutes=-10)


def add_10min(dt):
    return dt + timedelta(minutes=10)


def timedelta_to_10min(delta):
    return delta.days * 144 + delta.seconds / 600


if __name__ == '__main__':
    modify10min(path = os.path.abspath('out'))      # 文件夹目录

