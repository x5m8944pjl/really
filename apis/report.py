# coding:utf-8
# Author:Edison
# Time:2018-08-08 09:53
import os
import sys
import shutil
import datetime
import time
import warnings

from apis.transform_unit import LidarCorrection
from apis.general_config import Configure, log_config

sys.path.append(os.path.join(os.getcwd()))
warnings.filterwarnings("ignore")


class Transfromer(object):
    def __init__(self):
        self.cfg = Configure().readconf('dailyreport.conf')
        self.logger = log_config()
        self.buoy_names = [n for n in str(self.cfg.get('general', 'list_buoys')).split()]
        self.delete_tag = self.cfg.getboolean('general', 'delete_tag')

    def transfor(self, date, buoys):
        """
        :param buoys:[{'name': 'lidar1', 'id': 1, }, {'name': 'lidar2', 'id': 2}]
        将一定时间范围内的雷达数据校正为10min数据和raw级数据
        """
        path_corr = self.cfg.get('installation', 'root')
        path_out = os.path.join(path_corr, self.cfg.get('installation', 'outpath'))
        sensors = self.cfg.get('general', 'jardepend').split(' ')
        sensors_dict = {'lidar': 'LIDAR', 'trimble': 'Trimble', 'weather': 'Weather', 'coda': 'Motion'}
        suffix_d = ('.csv', '.ZPH', '.zip', 'txt')
        raw_path = self.cfg.get('installation', 'rawpath')
        t = date
        timestamp = time.mktime(time.strptime(date, "%Y_%m_%d"))
        _t = '{}-{}-{}'.format(t[:4], t[5:7], t[8:10])
        t_zph = 'Y{}_M{}_D{}'.format(t[:4], t[5:7], t[8:10])
        # 设置雷达参数
        for buoy in buoys:
            self.logger.info('handling {} {} '.format(t, buoy['name']) + 'data')
            # 读取lidar数据
            # 查重
            path_o_l = os.path.join(path_out, buoy['name'])
            dir_o_f = os.listdir(path_o_l)
            bo = False
            try:
                for f in dir_o_f:
                    if f.endswith('.csv'):
                        if ('{0}_{0}'.format(_t) in f) and (
                                (('10MinAveraged_corrected_{0}_{0}'.format(_t)) in f) or (
                                'corrected{0}_{0}'.format(_t) in f)):
                            bo = True
                            break
                        else:
                            bo = False
            except Exception as e:
                pass
            if bo is False:
                # 拷贝coda数据到
                for sensor in sensors:
                    path_s = os.path.join(path_corr, sensor)
                    path_l = os.path.join(path_corr, raw_path, buoy['name'], sensors_dict.get(sensor))
                    dir_l = os.listdir(path_l)
                    for f in dir_l:
                        if t in f or t_zph in f and not f.startswith('Wind10'):
                            path_f = os.path.join(path_l, f)
                            _path_f = os.path.join(path_s, f)
                            shutil.copyfile(path_f, _path_f)
                            self.logger.info("already selected file :{} to {} ".format(path_f, _path_f))
                            break
                # 转换lidar数据
                try:
                    LidarCorrection().loop()
                except Exception as e:
                    raise e
                    self.logger.error(e)
                finally:
                    # 不论是否校准成功，将缓存文件删除
                    if self.delete_tag is True:
                        for sensor in sensors:
                            path_s = os.path.join(path_corr, sensor)
                            dir_s = os.listdir(path_s)
                            for f in dir_s:
                                if f.endswith(suffix_d):
                                    path_f = os.path.join(path_s, f)
                                    os.remove(path_f)
                                    self.logger.info('already delete {}'.format(path_f))
                    # 文件归类
                    _path_out = os.path.join(path_out, buoy['name'])
                    dir_o = os.listdir(path_out)
                    for o in dir_o:
                        if o.endswith('.csv'):
                            path_f = os.path.join(path_out, o)
                            _path_f = os.path.join(_path_out, o)
                            shutil.move(path_f, _path_f)
                            self.logger.info('move {} to {}'.format(path_f, _path_f))
            else:
                self.logger.info("data of {} already exits.".format(t))
        self.logger.info("Transform done!")

    def transfer_daily(self, date):
        now = datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%Y_%m_%d')
        buoys = []
        for buoy_name in self.buoy_names:
            buoy = {}
            buoy['name'] = buoy_name
            buoy['id'] = buoy_name[4]
            buoys.append(buoy)
        self.transfor(date=now, buoys=buoys)


class Report(object):
    def __init__(self):
        self.logger = log_config('report.log')
        self.config = Configure()
        self.config.readconf('dailyreport.conf')

    def dreport_internal(self, date):
        # 检查原始数据
        # 输出10分钟报告和raw报告
        try:
            Transfromer().transfer_daily(date)
        except Exception as e:
            self.logger.error(e)
            raise e


if __name__ == '__main__':
    pass
