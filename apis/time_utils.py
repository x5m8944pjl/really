# coding=utf-8

from datetime import datetime, timedelta


def utc2cst(ts):
    ts = ts.replace('T', ' ')
    dt = datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
    dt = dt + timedelta(hours=8)
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def tstr2dt(time_string):
    ts = time_string.replace('T', ' ')
    try:
        return datetime.strptime(ts, "%Y-%m-%d %H:%M:%S")
    except:
        return datetime.strptime(ts, "%Y/%m/%d %H:%M:%S")


def dt2tstr(dt):
    return dt.strftime("%Y-%m-%d %H:%M:%S")


def minus_10min(dt):
    return dt + timedelta(minutes=-10)


def add_10min(dt):
    return dt + timedelta(minutes=10)


def timedelta_to_10min(delta):
    return delta.days*144 + delta.seconds/600
