import os
import shutil
import zipfile
import subprocess

import numpy as np
import pandas as pd

def drop_chinese(df, pattern='d'):
    """
    筛选出'Backscatter (/1.3e-6/m/sr)'列中 不含中文的DataFrame

    :param df: DataFrame
    :param pattern: 'd'-正则匹配数字，提取数字行; 'cn'-正则匹配中文，提取非中文行
    """
    # 筛选出所有object类型的列，里面可能类型混合
    ob = df.select_dtypes(include=['object'])

    # 转换成str类型
    obstr = ob.astype('str')
    index1 = []
    index2 = []
    if pattern == 'd':
        try:
            index1 = np.where(obstr['Backscatter (/1.3e-6/m/sr)'].str.extract('(\d)').isnull() == True)[0]
        except Exception as e:
            print(e)
        try:
            index2 = np.where(obstr['Wind Direction (deg)'].str.extract('(\d)').isnull() == True)[0]
        except Exception as e:
            print(e)
        inner = np.union1d(index1, index2)
        result = df.loc[inner]
    else:
        index1 = np.where(obstr['Backscatter (/1.3e-6/m/sr)'].str.extract('([\u4e00-\u9fa5])').isnull() == True)[0]
        index2 = np.where(obstr['Wind Direction (deg)'].str.extract('([\u4e00-\u9fa5])').isnull() == True)[0]
        inner = np.union1d(index1, index2)
        result = df.loc[inner]

    result = df.drop(result.index)
    return result


class LidarCorrection(object):
    def __init__(self):
        self.root = os.getcwd()
        self.lidar_root = os.path.join(self.root, 'lidar')
        self.conf_root = os.path.join(self.root, 'configs')
        self.lidar_zip_list = self._zip_list(os.listdir(self.lidar_root))
        self.csv_list = []

    @staticmethod
    def _fn(x):
        for i in x.index[10:24]:
            try:
                if x[i] > 5000:
                    x[i] = np.nan
            except:
                pass
        return x

    @staticmethod
    def _check_header_len(filename):
        with open(filename) as f:
            header = f.readline().split(',')
            header_len = len(header)
            if header_len < 10:
                return False
            else:
                return True

    @staticmethod
    def _zip_list(alist):
        al = []
        for f in alist:
            if f.endswith('.zip'):
                al.append(f)
        return al

    @staticmethod
    def rm(path):
        try:
            os.remove(path)
        except:
            pass

        try:
            os.rmdir(path)
        except:
            pass

        try:
            shutil.rmtree(path)
        except:
            pass

    def exec_jar(self):
        # try:
        #     os.system(
        #         'cd "{}" && java -cp ".;iweslidarcorr.jar"  "-Djava.library.path=."\
        #          -Xmx8g -XX:+UseG1GC de.fraunhofer.iwes.buoy.ZephIR'.format(self.root))
        # except Exception as e:
        #     print(e)
        cmd = (
            'java -cp ".;iweslidarcorr.jar"  "-Djava.library.path=." '
            '-Xmx8g -XX:+UseG1GC de.fraunhofer.iwes.buoy.ZephIR')
        try:
            output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, timeout=150)
        except subprocess.CalledProcessError as ce:
            raise ce
        except subprocess.TimeoutExpired as te:
            raise te

    def exists_csv(self):
        l_csv = []
        for f in os.listdir(self.lidar_root):
            if f.endswith('.csv'):
                l_csv.append(f)
        return l_csv

    def copy_app_conf(self, buoy_id, exists_list):
        app_conf_path = os.path.join(self.root, 'application.conf')
        app_conf_path_ = ''
        buoy_id_ = exists_list[0][5:8]
        if buoy_id is not None:
            app_conf_path_ = os.path.join(self.conf_root, 'application-{}.conf'.format(buoy_id))
        elif buoy_id_ is not None:
            print('lidar zph.zip file not fund, using csv')
            app_conf_path_ = os.path.join(self.conf_root, 'application-{}.conf'.format(buoy_id_))
        else:
            print('lidar files not fund')
        if os.path.exists(app_conf_path):
            os.remove(app_conf_path)
        shutil.copy(app_conf_path_, app_conf_path)

    def check_buoy(self):
        for i in self.lidar_zip_list:
            return str(i[5:8])

    def rm_useless(self):
        for f in os.listdir(self.lidar_root):
            if f.endswith(('.ZPH_', '.ZPH.csv')):
                self.rm(os.path.join(self.lidar_root, f))

    def uzip_all(self):
        for f in self.lidar_zip_list:
            try:
                unzip_path = os.path.join(self.lidar_root, f[:-4] + '_')
                csv_name = os.path.join(self.lidar_root, f[:-4] + '.csv')
                self.csv_list.append(csv_name)
                if not os.path.exists(csv_name):
                    zip_ref = zipfile.ZipFile(os.path.join(self.lidar_root, f), 'r')
                    zip_ref.extractall(unzip_path)
                    zip_ref.close()
                    shutil.move(os.path.join(unzip_path, f[:-4]), self.lidar_root)
                    self.rm(unzip_path)
            except Exception as e:
                print(e)

    def zph2csv(self):
        os.system(
            'cd "{}" && "ZPH2CSV.exe" . --horizontal --advanced'.format(
                self.lidar_root))

    def clean_new(self, csv):
        df = pd.read_csv(csv, sep=',', skiprows=1, low_memory=False)
        df['Fog'] = 0.001
        df.rename(columns={'Fog': u'Spatial Variation'}, inplace=True)
        df = df[list(df.columns[:-4]) + [df.columns[-2]] + list(df.columns[-4:-2]) + [df.columns[-1]]]
        try:
            df = drop_chinese(df)
        except Exception as e:
            print(e)
        f64 = df.select_dtypes(include=['float64'])
        fcols = f64.columns
        df.loc[:, fcols] = f64.round(3)
        df = df.apply(self._fn, axis=1)
        df.dropna(inplace=True)
        out_name = os.path.join(self.lidar_root, csv[:-8] + '.csv')
        df.to_csv(out_name, index=None)
        with open(csv, 'r+') as f1:
            header = f1.readline()
        with open(out_name, 'r+') as f2:
            content = f2.read()
            f2.seek(0, 0)
            f2.write(header + content)  # 取出第一行，写入outfile.csv

    def clean_old(self, csv):
        out_name = os.path.join(self.lidar_root, csv[:-8] + '.csv')
        shutil.copy(csv, out_name)

    def clean_all(self, exists_list):
        diff_list = [i for i in self.csv_list if i not in exists_list]
        for csv in diff_list:
            if self._check_header_len(csv):
                self.clean_new(csv)
            else:
                self.clean_old(csv)

    def loop(self):
        buoy_id = self.check_buoy()
        self.uzip_all()
        self.zph2csv()
        exists_list = self.exists_csv()
        self.copy_app_conf(buoy_id, exists_list)
        self.clean_all(exists_list)
        self.rm_useless()
        self.exec_jar()

def transfrom():
    lr = LidarCorrection()
    buoy_id = lr.check_buoy()
    lr.uzip_all()
    lr.zph2csv()
    exists_list = lr.exists_csv()
    lr.copy_app_conf(buoy_id, exists_list)
    lr.clean_all(exists_list)
    lr.rm_useless()
    lr.exec_jar()


def simplify_zephir_10min(filepath):
    with open(filepath, 'r') as f:
        first_line = f.readline()
        second_line = f.readline()
        third_line = f.readline()
    header = first_line + second_line + third_line
    df = pd.read_csv(filepath, header=3, sep='\t')
    after = df.filter(regex='UTC|Algorithms|horizontal|wind speed corrected|availability')
    after.to_csv(filepath, index=0, float_format='%.3f')

    with open(filepath, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(header + content)


def simplify_lidar_10min(filepath):
    with open(filepath, 'r') as f:
        header = f.readline()
    usecols = ['Time and Date',
               'Info. Flags',
               'Status Flags',
               'Battery (V)',
               'Upper Temp. (C)',
               'Pod Humidity (%)',
               'Met Tilt (deg)',
               'Met Pressure (mbar)',
               'Met Humidity (%)',
               'Met Wind Direction (deg)',
               'Proportion Of Packets With Rain (%)',
               'Points in Fit',
               'Packets in Average',
               'Wind Direction (deg)',
               'Horizontal Wind Speed (m/s)',
               'Height (m)',
               'Proportion Of Packets with Fog (%)',
              ]
    after = pd.read_csv(filepath, header=1, usecols=usecols)
    after.to_csv(filepath, index=0, float_format='%.3f')

    with open(filepath, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(header + content)


def simplify_and_compress_zephir_10min(src_path, des_dir):
    try:
        shutil.copy(src_path, des_dir)
        # simplify
        des_path = '{}/{}'.format(des_dir, os.path.basename(src_path))
        simplify_zephir_10min(des_path)
        # compress
        des_zip_path = '.'.join([des_path[:-4], '7z'])
        cmd_str = "7z a -t7z {} {} -mx=9".format(des_zip_path, des_path)
        os.system(cmd_str)
        os.remove(des_path)
    except Exception as e:
        print(e)


def simplify_and_compress_lidar_10min(src_path, des_dir):
    try:
        shutil.copy(src_path, des_dir)
        # unzip and zph2csv
        des_zph_zip_path = '{}/{}'.format(des_dir, os.path.basename(src_path))
        shutil.unpack_archive(des_zph_zip_path, des_dir)
        os.system('cd "{}" && "ZPH2CSV.exe" . --horizontal --advanced'.format(des_dir))
        # simplyfy
        des_csv_path = '.'.join([des_zph_zip_path[:-4], 'csv'])
        simplify_lidar_10min(des_csv_path)
        # compress
        des_7z_path = '.'.join([des_zph_zip_path[:-4], '7z'])
        cmd_str = "7z a -t7z {} {} -mx=9".format(des_7z_path, des_csv_path)
        os.system(cmd_str)
        # remove temp file
        os.remove(des_zph_zip_path)
        os.remove(des_zph_zip_path[:-4])
        os.remove(des_csv_path)
    except Exception as e:
        print(e)


if __name__ == '__main__':
    LidarCorrection().loop()
