import os
import sys
import smtplib
from email.mime.text import MIMEText
from email.header import Header

from apis.general_config import Configure, log_config
sys.path.append(os.path.join(os.getcwd()))

logger = log_config() 
 
cfg = Configure().readconf('dailyreport.conf')
smtp_server = cfg.get('email', 'smtp_server')
sender = cfg.get('email', 'mail_sender')
password = cfg.get('email', 'mail_password')
receivers = cfg.get('email', 'mail_receivers').split(',')

mail_msg_from = cfg.get('email', 'mail_msg_from')
mail_msg_subject = cfg.get('email', 'mail_msg_subject')

class MimeMsg(object):
    def __init__(self, text='', _from=mail_msg_from, subject=mail_msg_subject):
        self.mime = MIMEText(text, 'plain', 'utf-8')
        self.mime['From'] = Header(_from, 'utf-8')
        self.mime['Subject'] = Header(subject, 'utf-8')


class MailAgent(object):
    def __init__(self, smtp_server=smtp_server, server_port=25):
        self.logger = log_config()
        self.smtpObj = smtplib.SMTP(smtp_server, server_port)
        
    def send_mail(self, sender=sender, password=password, receivers=receivers, message=MIMEText('')):
        try:
            self.smtpObj.ehlo()
            self.smtpObj.starttls()
            self.smtpObj.login(sender, password)
            self.smtpObj.sendmail(sender, receivers, message.as_string())
            self.smtpObj.quit()
            self.logger.info("Mail sent success")
        except smtplib.SMTPException as e:
            self.logger.error(str(e))