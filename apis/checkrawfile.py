import os
import datetime

from apis.general_config import Configure, log_config

"""
判断LIDAR、Motion、Weather、Trimble文件夹是否正确
"""


class CheckDirList(object):
    """
    check dir list name(unnecessary)
    :return:
    """

    # 变量设置
    def __init__(self, date_):
        self.conf = Configure()
        self.logger = log_config()
        self.conf.readconf(confname='dailyreport.conf')
        self.path_z = self.conf.get('general', 'datapath')
        self.ts = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.date_ = date_
        self.list_buoy = [n for n in str(self.conf.get('general', 'list_buoys')).split()]
        self.list_out = []
        self.checkraw_list = [n for n in str(self.conf.get('general', 'checkraw_list')).split()]
        self.date = datetime.datetime.strptime(date_, '%Y-%m-%d')
        self.motion = self.date.strftime('COM1_%Y_%m_%d')
        self.lidar = self.date.strftime('Y%Y_M%m_D%d')
        self.trimble = self.date.strftime('PORT3001_%Y_%m_%d')
        self.weather = self.date.strftime('COM2_%Y_%m_%d')

    # 遍历文件
    def check_unit(self, dirname='Motion', root='', f=''):
        fullname_f = os.path.join(root, f)
        filesize = os.path.getsize(fullname_f)
        tag = 1
        if dirname == 'LIDAR':
            percent = int(self.conf.get(dirname, 'diffpercent'))
            if f.startswith('Wind_'):
                threshold = int(self.conf.get(dirname, 'threshold1'))
                if (filesize < (threshold * (100 + percent) / 100)) and (
                        filesize > (threshold * (100 - percent) / 100)):
                    self.logger.info('{} /{} size complete.'.format(dirname, f))
                    tag = 2
                return tag
            else:
                self.logger.info('{} /{} size uncomplete.'.format(dirname, f))
        else:
            threshold = int(self.conf.get(dirname, 'threshold'))
            percent = int(self.conf.get(dirname, 'diffpercent'))
            if (filesize < (threshold * (100 + percent) / 100)) and (filesize > (threshold * (100 - percent) / 100)):
                self.logger.info('{} /{} size complete.'.format(dirname, f))
                tag = 2
            else:
                self.logger.error('{} /{} size uncomplete.'.format(dirname, f))
            return tag

    def check_buoy(self, buoy):
        status_dict = {
            'tag': 1,
            'ts': self.ts,
            'date': self.date_,
            'motion': 0,
            'lidar': 0,
            'trimble': 0,
            'weather': 0,
            'buoy_id': 0,
        }
        path_l = os.path.join(self.path_z, buoy)
        status_dict['buoy_id'] = buoy[4:]
        # print (self.list_rawdir)
        for rawdir in self.checkraw_list:
            path_rd = os.path.join(path_l, rawdir)
            try:
                dir_f = os.listdir(path_rd)
                for f in dir_f:
                    if self.motion in f:
                        status_dict['motion'] = self.check_unit(dirname='Motion', root=path_rd, f=f)
                    if self.lidar in f:
                        if f.startswith('Wind_'):
                            status_dict['lidar'] = self.check_unit(dirname='LIDAR', root=path_rd, f=f)
                    if self.trimble in f:
                        status_dict['trimble'] = self.check_unit(dirname='Trimble', root=path_rd, f=f)
                    if self.weather in f:
                        status_dict['weather'] = self.check_unit(dirname='Weather', root=path_rd, f=f)
            except BaseException as e:
                self.logger.error(e)
        return status_dict

    def check_buoys(self):
        for buoy in self.list_buoy:
            status_dict = self.check_buoy(buoy)
            self.list_out.append(status_dict)
        return self.list_out

    def placeholder(self):
        n = 0
        list_out = self.check_buoys()
        for out in list_out:
            n = out['motion'] + out['lidar']
        return n



if __name__ == '__main__':
    cdl_ = CheckDirList('2018-08-07')
